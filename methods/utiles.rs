use std::convert::TryInto;

pub fn print_matrix(matrix: &mut Vec<Vec<f64>>)
{
	for (_i, _line) in matrix.iter().enumerate() {
		print!("[ ");
		for (_j, elem) in _line.iter().enumerate() {
			if elem >= &0.0
			{
				print!(" ");
			}
			print!("{:.2} ", elem);
		}
		println!(" ]");
	}
	println!("\n\n");
}

pub fn min_column(matrix: &mut Vec<Vec<f64>>, ignores_zeros: bool) -> Vec<f64>
{
	let mut min_column_vectr: Vec<f64> = vec![0f64; matrix.len()];
	for (i, _line) in matrix.iter().enumerate() {
		min_column_vectr[i] = f64::INFINITY;
		for (j, _elem) in _line.iter().enumerate() {
			if (matrix[i][j] < min_column_vectr[i]) && (i != j) && (!ignores_zeros || matrix[i][j]!=0.0)&& matrix[i][j]!=f64::INFINITY{
				min_column_vectr[i] = matrix[i][j];
			}
		}		
	}
	return min_column_vectr;

}

pub fn substract_from_colomns(array : Vec<f64>, matrix : &mut Vec<Vec<f64>>) {
	for i in 0..=matrix.len()-1 {
		print!("[ ");
		for j in 0..=matrix.len()-1 {
			if i != j && matrix[i][j] >= array[i] && matrix[i][j]!=f64::INFINITY
			{
				matrix[i][j] -= array[i];
			}
		}
	}
}

pub fn min_row(matrix: &mut Vec<Vec<f64>>, ignores_zeros: bool) -> Vec<f64>
{
	let mut min_column_vectr: Vec<f64> = vec![0f64; matrix.len()];
	for (i, _line) in matrix.iter().enumerate() {
		min_column_vectr[i] = f64::INFINITY;
		for (j, _elem) in _line.iter().enumerate() {
			if (matrix[j][i] < min_column_vectr[i]) && (i != j)&& (!ignores_zeros || matrix[j][i]!=0.0) && matrix[j][i]!=f64::INFINITY{
				min_column_vectr[i] = matrix[j][i];
			}
		}		
	}
	return min_column_vectr;

}

pub fn substract_from_rows(array : Vec<f64>, matrix : &mut Vec<Vec<f64>>) {
	for i in 0..=matrix.len()-1 {
		for j in 0..=matrix.len()-1{
			if i != j && matrix[j][i] >= array[i] && matrix[j][i]!=f64::INFINITY
			{
				matrix[j][i] -= array[i];
			}
		}
	}
}
use std::fs::File;
use std::io::{Write, BufReader, BufRead, Error};

use std::fmt::write;
//use std::fs::write;
//use std::ptr::write;

pub fn write_solution(solution : &[usize] , write_solution: &str, distance: f64){
	println!("{:?}", write_solution);
	let mut file = std::fs::File::create(write_solution).expect("create failed");
	//file.write_all(&*write_solution.as_bytes());
	for &elem in solution {
		file.write_all(elem.to_string().as_bytes());
		file.write_all(" ".as_bytes());
	}
	file.write_all(b"\n");
	file.write_all(distance.to_string().as_bytes());
}

pub fn calculate_distance(original_distance: &mut Vec<Vec<f64>>, solution : &[usize]) -> f64
{
	let mut current: usize = 0;
	let mut next: usize = 0;
	let mut j: usize = 0;
	let mut distance: f64 = 0.0;
	

	for i in 0..=solution.len()-1 {
		current = solution[i].try_into().unwrap();
		j = (i + 1).try_into().unwrap();
		if j == (solution.len()).try_into().unwrap(){
			break;
		}
		next = solution[j].try_into().unwrap();
		distance += original_distance[current][next];
	}
	
	
	
	return distance;
}
