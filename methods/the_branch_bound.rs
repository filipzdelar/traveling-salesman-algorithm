mod utiles;
use std::convert::TryInto;

pub fn the_branch_bound_solve(matrix: &mut Vec<Vec<f64>>, output_filename: &str){

    	println!("\n\tInput\n\n");
	utiles::print_matrix(matrix);

	let mut original_distance2 = vec![vec![0f64; matrix.len()]; matrix.len()];
	for (_i, _line) in matrix.iter().enumerate() {
		for (_j, elem) in _line.iter().enumerate() {
			original_distance2[_i][_j] = *elem;
			
			
			
		}
	}
	
	let mut original_distance = vec![vec![0f64; matrix.len()]; matrix.len()];
	for (_i, _line) in matrix.iter().enumerate() {
		for (_j, elem) in _line.iter().enumerate() {
			original_distance[_i][_j] = *elem;
			
			if _i==_j {
				original_distance[_i][_j] = f64::INFINITY;
			}
			
		}
	}
	
	let mut bestSolution: Vec<usize> = Vec::new();

	
	let mut begin: Vec<usize> = Vec::new();
	
	let mut BestSolution: Vec<usize> = Vec::new();
	    
	    	for i in 0..=matrix.len()-1 {
	    		begin.push(i.try_into().unwrap());    
	    	}
	
	let mut distance: f64 = f64::INFINITY;
	let mut temporary_solution: Vec<usize> = Vec::new();
	
	
	for i in 0..=begin.len()-1 {
		
			
		let mut original_distance = vec![vec![0f64; matrix.len()]; matrix.len()];
		for (_i, _line) in matrix.iter().enumerate() {
			for (_j, elem) in _line.iter().enumerate() {
				original_distance[_i][_j] = *elem;
				
				if _i==_j {
					original_distance[_i][_j] = f64::INFINITY;
				}
				
			}
		}
		
		//utiles::print_matrix(&mut original_distance);
		let mut ignore_zeros: bool = false;
		let mut first: i32 = 0;             
		let mut solution: Vec<i32> = Vec::new();
		let mut items: Vec<usize> = Vec::new();
		let mut results: Vec<usize> = Vec::new();
	    
	    	for i in 0..=matrix.len()-1 {
	    		items.push(i.try_into().unwrap());    
	    	}
	    		
		
		let mut cost: f64 = 0.0;
		solution.push(i.try_into().unwrap());
		let mut min_columns: Vec<f64> = utiles::min_column(&mut original_distance, ignore_zeros);
		//println!("min_columns {:?}", min_columns);
		utiles::substract_from_colomns(utiles::min_column(&mut original_distance, ignore_zeros),&mut original_distance);
			
		let mut min_rows: Vec<f64> = utiles::min_row(&mut original_distance, ignore_zeros);
		//println!("min_rows {:?}", min_rows);
		utiles::substract_from_rows(utiles::min_row(&mut original_distance, ignore_zeros),&mut original_distance);	
		
		//utiles::print_matrix(&mut original_distance);
		
		
			
		for i in 0..=min_rows.len()-1 {
			cost+=min_rows[i]+min_columns[i];
		}
		//println!("Cost Nodes Begin {:?}", cost);	
		
		
		items.remove(i.try_into().unwrap());
		
		
		
		
	    	let mut newElement: usize =  items[0].try_into().unwrap(); 
		while items.len() !=0 {
		
			
			
			let(mut temporary_distance,mut items,mut solution, cost) = OneNodeCost(&mut original_distance, &mut items, cost, &mut solution);
			/*for i in 0..=temporary_distance.len()-1  {
				for j in 0..=temporary_distance.len()-1  {
				original_distance[i][j] = temporary_distance[i][j];
				
					
				}
			}*/
			
			

	       
		}
		
		let mut solutionTemp: Vec<usize> = Vec::new();
			for i in 0..=solution.len()-1 {
				solutionTemp.push(solution[i].try_into().unwrap());
			}
			
			let mut dist: f64 = utiles::calculate_distance(matrix, &solutionTemp);	
			
			
			
			if dist < distance {
				distance = dist;
				bestSolution= solutionTemp;
			}
		//let mut temp_distance: f64 = f64::INFINITY;
			
		/*let mut solution2: Vec<usize> = Vec::new();
		for i in 0..=solution.len()-1 {
			solution2.push ( solution[i].try_into().unwrap());
		}
			
		
			
		temp_distance = utiles::calculate_distance(matrix, &solution2); 
			
		//utiles::print_matrix(matrix);

		println!("Cost{:?}", solution2);
		if (temp_distance < distance){
			
			distance = temp_distance;
			
			temporary_solution= solution2;
			
			
		
		
	 	}*/
		
	}
	
	
	
	println!("solution ac {:?}",bestSolution);	
	println!("distance ac {:?}",distance);	
		
	utiles::write_solution(&bestSolution, output_filename, distance)	
		
	
	
	
}

fn OneNodeCost<'a>(original_distance: &'a mut Vec<Vec<f64>>, items: &'a mut Vec<usize>, cost:  f64, solution: &'a mut Vec<i32>) -> (Vec<Vec<f64>>,&'a mut Vec<usize>,&'a mut Vec<i32>, f64)

{

	let mut costInside: f64 = f64::INFINITY;
	
	let mut ignore_zeros: bool = false;
	
	let mut resultsMatrix  = vec![vec![0f64; original_distance.len()]; original_distance.len()];
	
	let mut lastElement: usize= (solution.len()-1).try_into().unwrap(); 
	let mut lastElementSolution: usize=solution[lastElement].try_into().unwrap();
	
	let mut iTemp: usize = 0;	
	for i in 0..=items.len()-1{
	
		
		let mut newElement: usize=items[i].try_into().unwrap();
		let mut costInsideTemp: f64 = 0.0;
		let mut results  = vec![vec![0f64; original_distance.len()]; original_distance.len()];
		for (a, line) in original_distance.iter().enumerate() {
			for (b, elem) in line.iter().enumerate() {
				results[a][b] = *elem;	
			}
		}
		
		//println!("Result matrix after nodes");
		//utiles::print_matrix(&mut results);
		
		//let mut c: usize = *_line;
		
		//println!("lastElementSolution {:?}", lastElementSolution);
		//println!("newElement {:?}", newElement);
		
		let mut element_cost: f64 = results[lastElementSolution][newElement];
		
		results[lastElementSolution][newElement] = f64::INFINITY;	
		results[newElement][lastElementSolution] = f64::INFINITY;
		
		//println!("Result matrix 2 inf");
		//utiles::print_matrix(&mut results);
		for a in 0..=results.len()-1 {
			results[lastElementSolution][a] = f64::INFINITY;	
			results[a][newElement] = f64::INFINITY;	
					
					
				
			
		}
	
		//println!("Result coro inf");
		//utiles::print_matrix(&mut results);
		
		for i in 0..=solution.len()-1 {
			let mut help: usize = solution[i].try_into().unwrap();
			results[newElement][help] = f64::INFINITY;	
			
		}
		//println!("Solution i {:?}", solution);
		//println!("stupid");
		//utiles::print_matrix(&mut results);
		
		
	       
	        
		
		let mut min_columns: Vec<f64> = utiles::min_column(&mut results, ignore_zeros);
		
		utiles::substract_from_colomns(utiles::min_column(&mut results, ignore_zeros),&mut results);
		
		let mut min_rows: Vec<f64> = utiles::min_row(&mut results, ignore_zeros);
		utiles::substract_from_rows(utiles::min_row(&mut results, ignore_zeros),&mut results);	
	
		//println!("min_rows {:?}", &mut min_rows);
		//println!("min_columns {:?}", &mut min_columns);
		
		for i in 0..=min_rows.len()-1 {
			if min_rows[i] != f64::INFINITY{
				//println!("Cost bc {:?}",costInsideTemp);
				costInsideTemp += min_rows[i];
			}
			
			
		}
		
		//println!("Cost bc {:?}",costInsideTemp);
		
		
		
		
		
		for i in 0..=min_columns.len()-1 {
			if min_columns[i] != f64::INFINITY{
				
				costInsideTemp+=min_columns[i];
				
			}
			
		}
		//println!("Colo              1     {:?} ",min_columns);
		//println!("Cost ac {:?}",costInsideTemp);
	
		costInsideTemp += element_cost + cost;
		
			
		
		if costInsideTemp < costInside {
			
			costInside = costInsideTemp;
			
			iTemp=i;
			for (a, line) in results.iter().enumerate() {
				for (b, elem) in line.iter().enumerate() {
				resultsMatrix[a][b] = *elem;	
				}
			}
		}
		
	}	
		let mut a: usize = items[iTemp];
			 solution.push(a as i32);
			 items.retain(|&items| items != a );
			
			 
		
		
		//println!("resultsMatrix ac {:?}",resultsMatrix);
	        //println!("items ac {:?}",items);
		//println!("solution ac {:?}",solution);
    		//println!("costInside {:?}",costInside);
	
	
	
	(resultsMatrix, items, solution, costInside)

}
