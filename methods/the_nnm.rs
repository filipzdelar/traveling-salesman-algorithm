mod utiles;

pub fn the_nnm_solve(matrix: &mut Vec<Vec<f64>>, output_filename: &str) {
	let mut shortest_distance: f64 = 2000000.0;
	let mut shortest_path : Vec<usize> = Vec::new();

	for i in 0..=matrix.len()-1{
		let mut is_visited: Vec<Vec<usize>> = vec![vec![0usize; matrix.len()];matrix.len()];
		set_as_visited(&mut is_visited, i);
		let mut current : usize = i;
		let mut path : Vec<usize> = Vec::new();
		path.push(current);
		for j in 1..=matrix.len()-1 {
			let mut min_column = f64::INFINITY;
			let mut min_idex = 1000000; 
			for x in 0..=matrix[current].len() - 1 {
				if x != current && is_visited[current][x] == 0 && matrix[current][x] < min_column{
					min_idex = x;
					min_column = matrix[current][x];
				}	
			}

			set_as_visited(&mut is_visited, min_idex);
			current = min_idex;
			path.push(current);
		}

		let mut ith_distance : f64 = utiles::calculate_distance(matrix, &path);
		if shortest_distance > ith_distance{
			shortest_distance = ith_distance;
			shortest_path = path.clone();
		}

	}
	utiles::write_solution(&shortest_path, output_filename, shortest_distance);
}

fn set_as_visited(is_visited: &mut Vec<Vec<usize>>, index: usize)
{
	for i in 0..=is_visited.len()-1 {
		if i != index
		{
			is_visited[i][index] = 1;
		}
	}
}