mod utiles;
use std::convert::TryInto;
use std::collections::btree_set::{BTreeSet, IntoIter};

enum UniquePermutations {
    Leaf {
        elements: Option<Vec<usize>>,
    },
    Stem {
        elements: Vec<usize>,
        unique_elements: IntoIter<usize>,
        first_element: usize,
        inner: Box<Self>,
    },
}

impl UniquePermutations {
    fn new(elements: Vec<usize>) -> Self {
        if elements.len() == 1 {
            let elements = Some(elements);
            Self::Leaf { elements }
        } else {
            let mut unique_elements = elements
                .clone()
                .into_iter()
                .collect::<BTreeSet<_>>()
                .into_iter();

            let (first_element, inner) = Self::next_level(&mut unique_elements, elements.clone())
                .expect("Must have at least one item");

            Self::Stem {
                elements,
                unique_elements,
                first_element,
                inner,
            }
        }
    }

    fn next_level(
        mut unique_elements: impl Iterator<Item = usize>,
        elements: Vec<usize>,
    ) -> Option<(usize, Box<Self>)> {
        let first_element = unique_elements.next()?;

        let mut remaining_elements = elements;

        if let Some(idx) = remaining_elements.iter().position(|&i| i == first_element) {
            remaining_elements.remove(idx);
        }

        let inner = Box::new(Self::new(remaining_elements));

        Some((first_element, inner))
    }
}

impl Iterator for UniquePermutations {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::Leaf { elements } => elements.take(),
            Self::Stem {
                elements,
                unique_elements,
                first_element,
                inner,
            } => loop {
                match inner.next() {
                    Some(mut v) => {
                        v.insert(0, *first_element);
                        return Some(v);
                    }
                    None => {
                        let (next_fe, next_i) =
                            Self::next_level(&mut *unique_elements, elements.clone())?;
                        *first_element = next_fe;
                        *inner = next_i;
                    }
                }
            },
        }
    }
}


pub fn brute_force_solve(matrix: &mut Vec<Vec<f64>>, output_filename: &str) {

    utiles::print_matrix(matrix);
    let mut distance: f64 = 1000000000.0;
    
    let mut root: Vec<Vec<usize>> = Vec::new();
    let mut items: Vec<usize> = Vec::new();
    let mut current_perm: Vec<usize> = Vec::new();
    for i in 0..=matrix.len()-1 {
    	items.push(i.try_into().unwrap());    
    }
    
    for perm in UniquePermutations::new(items) {
        
        let mut current_distance: f64= utiles::calculate_distance(matrix, &perm);
        if current_distance < distance {
        	distance=current_distance;
        	println!("\n: {:?}", distance);
        	println!("{:?}", current_perm);
        	current_perm=perm;
        }
        
    }
    utiles::write_solution(&current_perm, output_filename, distance)

}