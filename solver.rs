use std::env;
use std::fs::File;
use std::io::{self, BufRead};


#[path = "methods/hunagian_methode.rs"] mod hunagian_methode;
#[path = "methods/brute_force.rs"] mod brute_force;
#[path = "methods/the_branch_bound.rs"] mod the_branch_bound;
#[path = "methods/the_nnm.rs"] mod the_nnm;
#[path = "methods/generic_algorithm.rs"] mod generic_algorithm;
#[path = "methods/utiles.rs"] mod utiles;


fn main() {
    let args: Vec<String> = env::args().collect();

    let broj_cvorova: usize = (&*args[2]).parse::<usize>().unwrap();
    //println!("{:?}", broj_cvorova);
    let f = io::BufReader::new(File::open("input_param").unwrap());

    let mut arr = vec![vec![0f64; broj_cvorova]; broj_cvorova];
    for (i, line) in f.lines().enumerate() {
    	for (j, number) in line.unwrap().split(char::is_whitespace).enumerate() {
        	let my_int: f64 = number.trim().parse::<f64>().unwrap();
        	//println!("{:?}", my_int);
            arr[i][j] = my_int;
        }
    }

    match &*args[1] {
        "BruteForce" => brute_force::brute_force_solve(&mut arr, &*args[3]),
        "TheBranchBound" => the_branch_bound::the_branch_bound_solve(&mut arr, &*args[3]),
        "TheNNM" => the_nnm::the_nnm_solve(&mut arr, &*args[3]),
        "HunagianMethode" => hunagian_methode::hunagian_methode_solve(&mut arr, &*args[3]),
        "GenericAlgorithm" => generic_algorithm::generic_algorithm_solve(&mut arr, &*args[3]),
        _ =>     println!("Metoda nije podrzana")
    }
}


	/*
	for (start_node, _line) in next_step.iter().enumerate() {
		turtle = start_node;
		rabbite = start_node;

		solution_found = true;

		println!("\n\n Start Node: {:?}", start_node);
		for (i, _line) in next_step.iter().enumerate() 
		{
			turtle = next_step[turtle];
			rabbite = next_step[next_step[rabbite]];
			println!("");
			println!("{:?}", turtle );
			println!("{:?}", rabbite );
			if(i != next_step.len() -1)
			{
				if(turtle == rabbite)
				{
					solution_found = false;
					break;
				}
			}

		}

		if(solution_found)
		{
			println!("");
			println!("Solution starts from");
			println!("");
			println!("{:?}", start_node);
			return true;
		}
	}*/
