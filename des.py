#!/bin/env python
"""
Created on Tue Dec 29 12:45:52 2020

@author: panonit
"""

import math

#lista = [[0,0], [0,1],[0,2],[0,3],[0,4],[1,4],[2,4],[3,4],[4,4],[4,3],[4,2],[4,1],[4,0],[3,0],[2,0],[1,0]]
#lista = [[1,1], [2,2],[4,4],[8,8]]
#lista = [[1,1], [4,4],[2,2],[8,8]]
#lista = [[0,0],[0,1],[5,0],[5,1]]
#lista = [[0,0],[1,1],[3,3]]
#lista = [[0,0], [1,3], [2,2],[0,1],[2,1],[2,0],[0,0], [1,3], [2,2],[0,1]]
#lista = [[0,0],[1,1]]
lista = [[0,0], [0,1],[0,2],[1,0],[1,1],[1,2],[2,0],[2,1],[2,2]]
matrica_udaljenosti = []

for cvor1 in range(len(lista)):
    udaljenost_od_covra1 = []
    for cvor2 in range(len(lista)):
        udaljenost_od_covra1.append(math.sqrt(math.pow(lista[cvor1][0] - lista[cvor2][0], 2) + math.pow(lista[cvor1][1] - lista[cvor2][1], 2)))
    matrica_udaljenosti.append(udaljenost_od_covra1)

with open('input_param', 'w') as f:
    for red in matrica_udaljenosti:
        for i, element in enumerate(red): 
            f.write("%s" % element if element != 0.0 else "-1")
            if i != (len(red)-1):
            	f.write(" ") 
        f.write("\n")

#BruteForce
#TheBranchBound
#TheNNM
#HunagianMethode
#GenericAlgorithm

#	[4, 0, 1, 4, 3]
#	[4, 3, 1, 4, 3]

import subprocess
subprocess.run([r"./solver", "TheBranchBound", str(len(lista)),"output_param"])


