import sys
 

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QInputDialog
from PyQt5.QtWidgets import QDesktopWidget
 
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
 
import numpy as np
import math
import subprocess
new=0

global x; 
global y;

import time

"""
def read_file():
    with open("output_param", "r") as f:
        SMRF1 = f.readlines()
    return SMRF1
    initial = read_file()
    while True:
        time.sleep(0.2)
        current = read_file()
        if initial != current:
            break
"""

def return_solution(list_x, list_y):
       solution_points = []
       with open("output_param", "r") as f:
              SMRF1 = f.readlines()
              print(SMRF1[1])
              solution = SMRF1[0].split(" ")[:-1]
              for i in solution:
                  solution_points.append([list_x[int(i)], list_y[int(i)]])
              print(solution_points)

class Window2(QMainWindow):
   def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.title = 'Results of the selected method'
        self.width = 1020
        self.height = 900
        self.setStyleSheet("background-color: black;") 
        self.initUI()
        m = PlotCanvas(self)
        m.move(240,0)
        
   def center(self):
       qr = self.frameGeometry()
       cp = QDesktopWidget().availableGeometry().center()
       qr.moveCenter(cp)
       self.move(qr.topLeft())   
       
   def changeNodes(self):
       self.close()
       self.w = App2()
       self.w.show()

   def hungarianMethodEvent(self):
       global x,y
       lista = []
       list_x = x[0]
       list_y = y[0]
       
       for i in range(len(list_x)):
           print(lista)
           point = []
           point.append(list_x[i])
           point.append(list_y[i])
           lista.append(point)
       matrica_udaljenosti = []

       for cvor1 in range(len(lista)):
           udaljenost_od_covra1 = []
           print("nes")
           for cvor2 in range(len(lista)):
               print("pet")
               udaljenost_od_covra1.append(math.sqrt(math.pow(lista[cvor1][0] - lista[cvor2][0], 2) + math.pow(lista[cvor1][1] - lista[cvor2][1], 2)))
           matrica_udaljenosti.append(udaljenost_od_covra1)

       with open('input_param', 'w') as f:
           for red in matrica_udaljenosti:
               for i, element in enumerate(red): 
                   f.write("%s" % element if element != 0.0 else "-1")
                   if i != (len(red)-1):
                   	f.write(" ") 
               f.write("\n")

       subprocess.run([r"./solver", "HunagianMethode", str(len(lista)),"output_param"])
       return_solution(list_x, list_y)

       
       
   def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.center()
        
        buttonTheBruteForceApproach = QPushButton('The Brute-Force Approach', self)
        buttonTheBruteForceApproach.setToolTip('The Brute-Force Approach')
        buttonTheBruteForceApproach.setStyleSheet("color: white;" "background-color: black;");
        buttonTheBruteForceApproach.move(0,0)
        buttonTheBruteForceApproach.resize(240,150)
      
        buttonTheBranchAndBoundMethod = QPushButton('The Branch and Bound Method', self)
        buttonTheBranchAndBoundMethod.setStyleSheet("color: white;" "background-color: black;");
        buttonTheBranchAndBoundMethod.setToolTip('The Branch and Bound Method')
        buttonTheBranchAndBoundMethod.move(0,150)
        buttonTheBranchAndBoundMethod.resize(240,150)
        
        buttonTheNearestNeighborMethod = QPushButton('The Nearest Neighbor Method', self)
        buttonTheNearestNeighborMethod.setStyleSheet("color: white;" "background-color: black;");
        buttonTheNearestNeighborMethod.setToolTip('The Nearest Neighbor Method')
        buttonTheNearestNeighborMethod.move(0,300)
        buttonTheNearestNeighborMethod.resize(240,150)
        
        buttonHungarianMethod = QPushButton('Hungarian Method', self)
        buttonHungarianMethod.setStyleSheet("color: white;" "background-color: black;");
        buttonHungarianMethod.setToolTip('Hungarian Method')
        buttonHungarianMethod.move(0,450)
        buttonHungarianMethod.resize(240,150)
        buttonHungarianMethod.clicked.connect(self.hungarianMethodEvent) 
        
        buttonGeneticAlgorithm = QPushButton('Genetic Algorithm', self)
        buttonGeneticAlgorithm.setStyleSheet("color: white;" "background-color: black;");
        buttonGeneticAlgorithm.setToolTip('Genetic Algorithm')
        buttonGeneticAlgorithm.move(0,600)
        buttonGeneticAlgorithm.resize(240,150)
        
        buttonForChangingNumberOfNodes = QPushButton('Change Number Of Nodes', self)
        buttonForChangingNumberOfNodes.setStyleSheet("color: white;" "background-color: black;");
        buttonForChangingNumberOfNodes.setToolTip('Change Number Of Nodes')
        buttonForChangingNumberOfNodes.move(0,750)
        buttonForChangingNumberOfNodes.resize(240,150)
        buttonForChangingNumberOfNodes.clicked.connect(self.changeNodes)            
        
class App2(QWidget):
 
    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.title = 'Results of the selected method'
        self.width = 1020
        self.height = 900
        self.initUI()
    
    def center(self):
       qr = self.frameGeometry()
       cp = QDesktopWidget().availableGeometry().center()
       qr.moveCenter(cp)
       self.move(qr.topLeft())
       
    def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.center()
        self.setWindowTitle(self.title)
        self.getInteger()
        
    def window2(self):        
        self.close()                                    
        self.w = Window2()
        self.w.show()
        
        
    def app2(self):                                             
        self.w = App2()
        self.w.show()
        self.hide()
        
    def getInteger(self):
        dlg =  QInputDialog(self)                 
        dlg.setInputMode( QInputDialog.IntInput)                
        dlg.resize(500,500)         
        dlg.setIntMinimum(0)
        dlg.setIntMaximum(100)
        dlg.setLabelText("Enter number of nodes:")                    
        okPressed = dlg.exec_()                                
        i = dlg.intValue()
      
        global new
        new=i
        print(new)
        if okPressed:
            print(new)
            self.window2()
        else:
            self.close()
            
class PlotCanvas(FigureCanvas):
 
    print(new)
    def __init__(self, parent=None, width=100, height=100):
        fig = Figure(figsize=(width, height))
        self.axes = fig.add_subplot(111)
        FigureCanvas.__init__(self, fig)
        
        self.setGeometry(50,50,800,750)
       
        self.setParent(parent)
        self.plot()
       
 
    def plot(self):
        print(new)
        global x,y
        x = np.random.randint(0,100,(1,new))  
        print(x)
        
        y = np.random.randint(0,100,(1,new))  
        ax = self.figure.add_subplot(111)
        ax.scatter(x,y)
        ax.set_title('Plot')
        self.draw()
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    ex = App2()


    