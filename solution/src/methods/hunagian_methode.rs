mod utiles;

pub fn hunagian_methode_solve(matrix: &mut Vec<Vec<f64>>, output_filename: &str) {


	println!("\n\tInput\n\n");
	utiles::print_matrix(matrix);

	let mut original_distance = vec![vec![0f64; matrix.len()]; matrix.len()];
	for (_i, _line) in matrix.iter().enumerate() {
		for (_j, elem) in _line.iter().enumerate() {
			original_distance[_i][_j] = *elem;
		}
	}

	println!("Nesto");
	println!("{:?}", matrix);
	println!("{:?}", original_distance);
	let mut circle_found: bool = false;
	let mut ignore_zeros: bool = false;
	let mut i: i32 = 0;
	while !circle_found && i < 5{
		i +=1;

		if i % 2 == 1{
			//println!("{:?}", utiles::min_column(matrix, true));
			utiles::substract_from_colomns(utiles::min_column(matrix, ignore_zeros), matrix);
		}else{
			//println!("{:?}", utiles::min_row(matrix, true));
			utiles::substract_from_rows(utiles::min_row(matrix, ignore_zeros), matrix);
		}

		//println!("After");
		utiles::print_matrix(matrix);
		//println!("Is there sircle:");
		circle_found = find_circle2(matrix, output_filename, &mut original_distance);
		//println!("{:?}", circle_found);
		ignore_zeros = true;
	}

	
    println!("Resault of HunagianMethode.");
}

fn find_circle2(matrix: &mut Vec<Vec<f64>>, output_filename: &str, original_distance: &mut Vec<Vec<f64>>) ->bool
{
	
	let mut next_step: Vec<Vec<usize>> = Vec::new();

	for (i, _line) in matrix.iter().enumerate() {
		let mut step: Vec<usize> = Vec::new();
		for (j, _elem) in _line.iter().enumerate() {
			if (matrix[i][j] == 0.0) && (i != j){
				step.push(j);
			}
		}	
		next_step.push(step);	
	}	
	//println!("{:?}", next_step);
	
	let mut next_step_list: Vec<Vec<usize>> = print_three_of_possibilities(&mut next_step, 0);
	//println!("{:?}", next_step_list);


	for next_step in next_step_list {
		let mut solution_found: bool;
		for (start_node, _line) in next_step.iter().enumerate() 
		{
			let mut previous_steps: Vec<usize> = vec![0usize; matrix.len()];
			//let mut previous_steps: Vec<usize> = vec![20000000usize; matrix.len()];

			for i in 0..=previous_steps.len()-1 {
				previous_steps[i] = 20000000;
			}

			let mut step = start_node;
			solution_found = true;
			previous_steps[0] = start_node;
			//println!("");
			//println!("{:?}", start_node);
			for i in 1..=next_step.len()-1{
				step = next_step[step];
				if previous_steps.contains(&step)  {
					solution_found = false;
					break;
				}
				previous_steps[i] = step;
				//println!("{:?}", previous_steps);
			}
			if solution_found
			{
				//println!("{:?}", previous_steps);
				let distance = utiles::calculate_distance(original_distance, &previous_steps);
				utiles::write_solution(&previous_steps, output_filename, distance);
				return true;
			}
			else {
				//println!("Next iteration");
			}
		}
	}
	return false;
}

fn print_three_of_possibilities(matrix: &mut Vec<Vec<usize>>, start_idex: usize) -> Vec<Vec<usize>>
{
	if start_idex == matrix.len()-1 
	{
		let mut root: Vec<Vec<usize>> = Vec::new();
		
		for element in &matrix[start_idex] {
			let mut leafs: Vec<usize> = Vec::new();
			leafs.push(*element);
			root.push(leafs); 
		}
		//println!("\nRoot: {:?}", root);
		return  root;
	}
	else{
		let  sub_permutation: Vec<Vec<usize>> = print_three_of_possibilities(matrix, start_idex+1);
		let mut permutation: Vec<Vec<usize>> = Vec::new();
		for element in &matrix[start_idex] {
			for sub_vector in &sub_permutation {
				let mut new_vector: Vec<usize> = Vec::new();
				new_vector.push(*element);
				for b in sub_vector {
					new_vector.push(*b);
				}
				permutation.push(new_vector);
			}
		}
		//println!("\nVertec: {:?}", permutation);
		return permutation;
	}
}
