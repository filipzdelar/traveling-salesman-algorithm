mod utiles;
mod quick_sort;
extern crate rand;
use rand::thread_rng;
use permutation_iterator::Permutor;


use rand::{distributions::Uniform, Rng}; // 0.6.5

pub fn generic_algorithm_solve(matrix:  &mut Vec<Vec<f64>>, output_filename: &str) {
    
    let population_size: usize = 128; //24;
    let epoch: usize = 2500; //10
    let elitisam: usize = 2; // First 3
    let crossover_parents: usize = 50; // First 10
    let crossover_childs: usize = crossover_parents + elitisam; // Creates new 13
    let mutation_size: usize = 65; // From  crossover_parents + crossover_childs - elitisam
    let mutation_ration: usize = 4*matrix.len();///10; // Nuber of mutations per child
    
    let mut population: Vec<Vec<usize>> = vec![vec![0usize; matrix.len()]; population_size];
    
    create_population(population_size, &mut population);
    rang_population(&mut population, matrix);
    
    for epoch in 0..=epoch
    {
        println!("EPOCHE: {}", epoch);
        create_new_population(&mut population, elitisam, crossover_parents, crossover_childs, mutation_size, mutation_ration);
        rang_population(&mut population, matrix);

    }
    
    let distance = utiles::calculate_distance(matrix, &population[0]);
    utiles::write_solution(&population[0], output_filename, distance);
    //utiles::print_matrix_u64(&mut population);
    //println!("Resault of GenericAlgorithm.");

}

fn create_population(size_of_population: usize, population: &mut Vec<Vec<usize>>) 
{
    for i in 0..=size_of_population-1{
        create_unit(i, population);
    }
}

fn create_unit(i: usize, population: &mut Vec<Vec<usize>>)
{
    let permutor = Permutor::new(population[0].len() as u64);
    for (index, permuted) in permutor.enumerate() {
        population[i][index] = permuted as usize;

    }
}

fn rang_population(population: &mut Vec<Vec<usize>>, original_distance: &mut Vec<Vec<f64>>)
{
    let mut rang: Vec<f64> = Vec::new();// vec![0f64; original_distance.len()];
    //let mut previous_steps: Vec<usize> = vec![0usize; original_distance.len()];

    for u in 0..=population.len()-1{
        let ul = utiles::calculate_distance(original_distance, &population[u]);
        rang.push(ul);
    }

    quick_sort::quick_sort(&mut rang, 0, population.len()-1, population);
    println!("{:?}", rang[0]);
}

fn create_new_population(population: &mut Vec<Vec<usize>>, elitisam: usize, crossover_parents: usize, crossover_childs: usize, mutation_size: usize, mutation_ration:usize)
{

    let mut chiled_population: Vec<Vec<usize>> = Vec::new();
    let mut chiled_mutant_population: Vec<Vec<usize>> = Vec::new();
    let mut rng = thread_rng();
    println!(" ");
    for i_chiled in 0..=crossover_childs-1 {
    	let parent_1 = rng.gen_range(0.. crossover_parents-1);
        let parent_2 = rng.gen_range(0.. crossover_parents-1);
        let g1 = rng.gen_range(2.. (population[0].len() - 3));
        //println!(" {} {}", parent_1, parent_2);
        chiled_population.push(make_chiled_cross_parents(population, parent_1, parent_2, g1));
    }
    // Add childern
    for c in 0..=chiled_population.len() -1 {
        for g in 0..=chiled_population[0].len() -1 {
            population[c+elitisam-1][g] = chiled_population[c][g];
        }
    }



    // Add mutation childern
    for i_chiled in 0..=mutation_size-1 {
        chiled_mutant_population.push(mutacion(&mut chiled_population, mutation_ration));
    }

    for c in 0..=chiled_mutant_population.len() -1 {
        for g in 0..=chiled_mutant_population[0].len() -1 {
            population[c+elitisam+chiled_population.len()-1][g] = chiled_mutant_population[c][g];
        }
    }
    //println!("{:?}",chiled_population);
}

fn make_chiled_cross_parents(population: &mut Vec<Vec<usize>>, parent_1: usize, parent_2: usize, g1: usize) -> Vec<usize>
{
    let mut child: Vec<usize> = Vec::new();

    //println!("P {} {} {}", parent_1, parent_2, g1);

    for i in 0..=g1 {
        child.push(population[parent_1][i]);
    }

    // Ako se nalazi do tada, upisi
    for k in 0..= population[0].len() - 1 {
        let mut exist = false;
        for l in  0..= child.len()-1{
            if(population[parent_2][k] == child[l] && exist == false)
            {
                exist = true;
            }
        }
        if(!exist)
        {
            child.push(population[parent_2][k]);
        }
    }

    return child;
}

fn mutacion(childern: &mut Vec<Vec<usize>>, mutation_size: usize)-> Vec<usize>
{
    let mut rng = thread_rng();
    let child_1 = rng.gen_range(0.. childern.len()-1);
    let mut mh = Vec::new();
    for i in 0..=mutation_size{
        let gen1 = rng.gen_range(0.. childern[0].len()-1);
        let gen2 = rng.gen_range(0.. childern[0].len()-1);
        swap(&mut childern[child_1], gen1, gen2)
    }
    for i in 0..=childern[child_1].len()-1 {
        mh.push(childern[child_1][i]);
    }
    return mh;
}

fn swap(childern: &mut Vec<usize>, gen1: usize, gen2: usize)
{
    let tmp = childern[gen1];
    childern[gen1] = childern[gen2];
    childern[gen2] = tmp;
}




/*

    let population_size: usize = 128; //24;
    let epoch: usize = 5000; //10
    let elitisam: usize = 2; // First 3
    let crossover_parents: usize = 10; // First 10
    let crossover_childs: usize = crossover_parents + elitisam; // Creates new 13
    let mutation_size: usize = 10; // From  crossover_parents + crossover_childs - elitisam
    let mutation_ration: usize = matrix.len()/2; // Nuber of mutations per child

*/



	//utiles::print_matrix(matrix);
