fn swap(param_int_array: &mut [f64], i: usize, j: usize,  population: &mut Vec<Vec<usize>>) {
    let temp = param_int_array[i];
    param_int_array[i] = param_int_array[j];
    param_int_array[j] = temp;

    for el in 0..=population[0].len()-1{
        let tempVec =  population[i][el];
        population[i][el] = population[j][el];
        population[j][el] = tempVec;
    }
}

fn partition(param_int_array: &mut [f64], start: usize, end: usize, population: &mut Vec<Vec<usize>> ) -> f64 {
    let pivot = param_int_array[end];
    let mut index = start;
 
    let mut i = start;
    while i < end {
        if param_int_array[i] < pivot {
            swap(param_int_array, i, index, population);
            index+=1;
        }
 
        i+=1;
    }
    swap(param_int_array, index, end, population);
    return index as f64;
}

pub fn quick_sort(param_int_array: &mut [f64], start: usize, end: usize, population: &mut Vec<Vec<usize>>) {
    if start >= end {
        return;
    }
    let pivot = partition(param_int_array, start, end, population);
 
    quick_sort(param_int_array, start, (pivot - 1.0) as usize, population);
    quick_sort(param_int_array, (pivot + 1.0) as usize, end, population);
}